const baseURL = 'https://boxinatorapi.azurewebsites.net'

export const AccountAPI = {
    async getAccountInfo(jwt){
        const requestOptions =  {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const user = await fetch(`${baseURL}/api/Accounts/account_id`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        console.log(user)
        return user
    },

    async login(jwt){
        const requestOptions =  {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const user = await fetch(`${baseURL}/api/Accounts/Login`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return user
    },
}
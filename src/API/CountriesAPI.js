const baseURL = 'https://boxinatorapi.azurewebsites.net'

export const CountriesAPI = {
    async getCountries(){
        const countries = await fetch(`${baseURL}/api/Countries`)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return countries
    },

    async updateCountry({ editedCountry, token}){
        const requestOptions =  {
            method: 'PUT',
            body: JSON.stringify(editedCountry),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const updateCountry = await fetch(`${baseURL}/api/Countries/${editedCountry.id}`, requestOptions)
            .then(response => response)
            .catch((error) => {
                console.log(error);
            })

        return updateCountry
    },

    async addCountry({countryName, multiplier}){
        const requestOptions =  {
            method: 'POST',
            body: JSON.stringify({
                "name": countryName,
                "multiplier": multiplier
            }),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const newCountry = await fetch(`${baseURL}/api/Countries`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return newCountry
    }
}
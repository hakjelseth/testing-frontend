const baseURL = 'https://boxinatorapi.azurewebsites.net'

export const ShipmentsAPI = {
    async getAccountShipments(jwt){
        const requestOptions =  {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const shipments = await fetch(`${baseURL}/api/Shipments`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return shipments
    },

    async getCompleteAccountShipments(jwt){
        const requestOptions =  {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const completeShipments = await fetch(`${baseURL}/api/Shipments/complete`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return completeShipments
    },

    async orderPackage({ recName, weight, color, country, token}){
        const requestOptions =  {
            method: 'POST',
            body: JSON.stringify({
                "accountID": "string",
                "receiverName": recName,
                "weight": weight,
                "boxColor": color,
                "countryName": country
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        const order = await fetch(`${baseURL}/api/Shipments`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return order
    },

    async deleteShipment(jwt, shipmentId){
        const requestOptions =  {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const deletedShipment = await fetch(`${baseURL}/api/Shipments/${shipmentId}`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        return deletedShipment
    },

    async updateShipment({ editedShipment, token}){
        const requestOptions =  {
            method: 'PUT',
            body: JSON.stringify({
                "id": editedShipment.id,
                "receiverName": editedShipment.receiverName,
                "weight": editedShipment.weight,
                "boxColor": editedShipment.boxColor,
                "countryName": editedShipment.countryName,
                "status": editedShipment.status
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        const updatedShipment = await fetch(`${baseURL}/api/Shipments/${editedShipment.id}`, requestOptions)
            .then(response => response)
            .catch((error) => {
                console.log(error);
            })

        return updatedShipment
    }
}
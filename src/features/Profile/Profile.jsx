import withKeycloak from "../../hoc/withKeycloak"
import KeycloakService from "../../services/KeycloakService"
import {useEffect, useState} from "react";
import {AccountAPI} from "../../API/AccountAPI";


const Profile = () => {
	const [profile, setProfile] = useState({})
	const username = KeycloakService.getUsername()
	const token = KeycloakService.getToken()
	const iss = KeycloakService.getIss();
	const azp = KeycloakService.getAzp();
	const profileLink = iss + "/account?referrer=" + azp;

	useEffect(async () => {
		let tempData = (await AccountAPI.getAccountInfo(token))
		setProfile(tempData)
	}, []);

	return (
		<div className="container border p-3 my-3">
			<main>
				<h1>Welcome to your profile, { username }</h1>
				<ul>
					{Object.keys(profile).map((key, index) => (
							<p key={index}> {key}: {profile[key]}</p>
						))}
				</ul>
				<a href={profileLink}>Edit profile</a>
			</main>
		</div>
	)
}
export default withKeycloak(Profile)
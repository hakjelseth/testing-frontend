import React from 'react';
import { Route, Redirect } from "react-router-dom";
import KeycloakService from "../../services/KeycloakService";

const AdminRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        KeycloakService.hasRole(["Administrator"]) === true
            ? <Component {...props} />
            : <Redirect to='/dashboard' />
    )} />
)

export default AdminRoute;
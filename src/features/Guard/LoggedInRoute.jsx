import React from 'react';
import { Route, Redirect } from "react-router-dom";
import KeycloakService from "../../services/KeycloakService";

const UserRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        KeycloakService.isLoggedIn() === false
            ? <Component {...props} />
            : <Redirect to='/dashboard' />
    )} />
)

export default UserRoute;
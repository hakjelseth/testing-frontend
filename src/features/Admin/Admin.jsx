import withKeycloak from "../../hoc/withKeycloak"
import KeycloakService from "../../services/KeycloakService"
import React, { useEffect, useState, Fragment } from "react";
import ReadOnlyShipmentsRow from "../Rows/ReadOnlyShipmentsRow";
import EditableShipmentsRows from "../Rows/EditableShipmentsRows";
import ReadOnlyCountriesRow from "../Rows/ReadOnlyCountriesRow";
import {useDispatch, useSelector} from "react-redux";
import {
	actionCountriesFetchAll,
	actionCountriesSet
} from "../../store/Actions/countriesActions";
import EditableCountriesRows from "../Rows/EditableCountriesRows";
import {actionShipmentsFetchAll, actionShipmentsSet} from "../../store/Actions/shipmentsActions";
import Modal from "react-modal";
import {ShipmentsAPI} from "../../API/ShipmentsAPI";
import {CountriesAPI} from "../../API/CountriesAPI";

const customStyles = {
	content: {
		background: 'cornflowerblue',
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
};

const Dashboard = () => {
	const dispatch = useDispatch()
	const { countries } = useSelector(state => state.countries)
	const { shipments } = useSelector(state => state.shipments)
	const username = KeycloakService.getUsername()
	const token = KeycloakService.getToken()
	const [editFormData, setEditFormData] = useState({
		receiverName: "",
		weight: "",
		boxColor: "",
		status: "",
		countryName: "",
		cost: 0
	});

	const [editShipmentId, setEditShipmentId] = useState(null);

	let subtitle;
	const [modalIsOpen, setIsOpen] = React.useState(false);
	Modal.setAppElement('#root');


	function openModal() {
		setIsOpen(true);
	}
	function afterOpenModal() {
		// references are now sync'd and can be accessed.
		subtitle.style.color = '#ffffff';
	}

	function closeModal() {
		setIsOpen(false);
	}

	useEffect( () => {
		if (countries.length === 0)
			dispatch(actionCountriesFetchAll())
		if (shipments.length === 0)
			dispatch(actionShipmentsFetchAll())
	}, []);

	const handleEditClick = (event, shipment) => {
		event.preventDefault();
		setEditShipmentId(shipment.id);

		const formValues = {
			receiverName: shipment.receiverName,
			weight: shipment.weight,
			boxColor: shipment.boxColor,
			status: shipment.status,
			countryName: shipment.countryName,
			cost: shipment.cost
		};

		setEditFormData(formValues);
	};

	const handleDeleteClick = async (shipmentId) => {
		const newShipments = [...shipments];

		const index = shipments.findIndex((shipment) => shipment.id === shipmentId);

		await ShipmentsAPI.deleteShipment(token, shipmentId);

		newShipments.splice(index, 1);

		//setShipments(newShipments);
		dispatch(actionShipmentsSet(newShipments))
	};

	const handleCancelClick = () => {
		setEditShipmentId(null);
	};

	const handleEditFormChange = (event) => {
		event.preventDefault();

		const fieldName = event.target.getAttribute("name");
		const fieldValue = event.target.value;

		const newFormData = { ...editFormData };
		newFormData[fieldName] = fieldValue;

		setEditFormData(newFormData);
	};

	const handleEditFormSubmit = async (event) => {
		event.preventDefault();

		const editedShipment = {
			id: editShipmentId,
			accountId: "string",
			receiverName: editFormData.receiverName,
			weight: editFormData.weight,
			boxColor: editFormData.boxColor,
			status: editFormData.status,
			countryName: editFormData.countryName,
			cost: editFormData.cost
		};

		const newShipments = [...shipments];

		const index = shipments.findIndex((shipment) => shipment.id === editShipmentId);

		await ShipmentsAPI.updateShipment({editedShipment, token})

		newShipments[index] = editedShipment;

		//setShipments(newShipments);
		dispatch(actionShipmentsFetchAll())
		setEditShipmentId(null);
	};


	const [editCountriesFormData, setCountriesEditFormData] = useState({
		name: "",
		multiplier: 0
	});

	const [editCountryId, setEditCountryId] = useState(null);

	const handleCountriesEditClick = (event, country) => {
		event.preventDefault();
		setEditCountryId(country.id);

		const formValues = {
			name: country.name,
			multiplier: country.multiplier
		};

		setCountriesEditFormData(formValues);
	};


	const handleCountriesCancelClick = () => {
		setEditCountryId(null);
	};

	const handleCountriesEditFormChange = (event) => {
		event.preventDefault();

		const fieldName = event.target.getAttribute("name");
		const fieldValue = event.target.value;

		const newFormData = { ...editCountriesFormData };
		newFormData[fieldName] = fieldValue;

		setCountriesEditFormData(newFormData);
	};

	const handleCountriesEditFormSubmit = async (event) => {
		event.preventDefault();

		const editedCountry = {
			name: editCountriesFormData.name,
			multiplier: editCountriesFormData.multiplier
		};

		const newCountries = [...countries];

		const index = countries.findIndex((country) => country.id === editCountryId);

		await CountriesAPI.updateCountry({editedCountry})

		newCountries[index] = editedCountry;

		dispatch(actionCountriesSet(newCountries));
		setEditCountryId(null);
	};

	const addNewCountry = async (evt) => {
		evt.preventDefault();
		let countryName = evt.target.countryName.value
		let multiplier = evt.target.multiplier.value
		await CountriesAPI.addCountry({ countryName, multiplier})
		dispatch(actionCountriesFetchAll())
		closeModal();
	}


	return (
		<div className="container border p-3 my-3">
			<main>
				<div>
					<h1>Welcome to the Admin page, { username }</h1>
				</div>
				<h3>ALL SHIPMENTS</h3>
				<form onSubmit={handleEditFormSubmit}>
					<table className="table">
						<tbody>
						<tr>
							<th>Box nr</th>
							<th>Receiver</th>
							<th>Weight</th>
							<th>Box color</th>
							<th>Status</th>
							<th>Country</th>
							<th>Cost</th>
						</tr>
						{ shipments.map((shipment, index) =>
							<Fragment key={index}>
								{editShipmentId === shipment.id ? (
									<EditableShipmentsRows
										editFormData={editFormData}
										index={index}
										countries={countries}
										handleEditFormChange={handleEditFormChange}
										handleCancelClick={handleCancelClick}
									/>
								) : (
									<ReadOnlyShipmentsRow
										shipment={shipment}
										index={index}
										handleEditClick={handleEditClick}
										handleDeleteClick={handleDeleteClick}
									/>
								)}
							</Fragment>
						)}
						</tbody>
					</table>
				</form>


				<h3>ALL COUNTRIES</h3>
				<form onSubmit={handleCountriesEditFormSubmit}>
					<table className="table">
						<tbody>
						<tr>
							<th>Name</th>
							<th>Multiplier</th>
						</tr>
						{ countries.map((country, index) =>
							<Fragment key={index}>
								{editCountryId === country.id ? (
									<EditableCountriesRows
										editFormData={editCountriesFormData}
										countries={countries}
										handleEditFormChange={handleCountriesEditFormChange}
										handleCancelClick={handleCountriesCancelClick}
									/>
								) : (
									<ReadOnlyCountriesRow
										country={country}
										handleEditClick={handleCountriesEditClick}
									/>
								)}
							</Fragment>
						) }
						</tbody>
					</table>
				</form>
				<button type="button" className="btn btn-primary btn-lg" onClick={openModal}>Add new country</button>
				<div>
					<Modal
						isOpen={modalIsOpen}
						onAfterOpen={afterOpenModal}
						onRequestClose={closeModal}
						style={customStyles}
						contentLabel="New country"
					>
						<h2 ref={(_subtitle) => (subtitle = _subtitle)}>Add new country</h2>

						<div>Enter details for country</div>
						<form onSubmit={addNewCountry} className="orderForm">
							<input type="text" name="countryName"  placeholder="Enter country name"/>
							<input type="number" name="multiplier"  placeholder="Enter multiplier"/>
							<button type="submit" className="shipButtons" id="sendButton">Send</button>
							<button className="shipButtons" id="closeButton" onClick={closeModal}>Close</button>
						</form>
					</Modal>
				</div>
			</main>
		</div>

	)
}
export default withKeycloak(Dashboard)
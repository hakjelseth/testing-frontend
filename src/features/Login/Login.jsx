import { Redirect } from "react-router-dom"
import KeycloakService from "../../services/KeycloakService"

const Login = () => {

	if (KeycloakService.isLoggedIn()) {
		return <Redirect to="/dashboard" />
	}

	const handleLoginClick = () => {
		KeycloakService.doLogin()
	}

	return (
		<div className="container border p-3 my-3">
			<main className="d-flex flex-column align-items-center align-items-xl-center">
				<h1>Welcome to the Boxinator</h1>
				<p>Ship a mystery box right to your door!</p>
				<button className="btn btn-dark mt-2" onClick={ handleLoginClick }>Login to Boxinator</button>
			</main>
		</div>
	)
}
export default Login
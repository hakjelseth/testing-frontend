import withKeycloak from "../../hoc/withKeycloak"
import KeycloakService from "../../services/KeycloakService"
import React, {useEffect, useState} from "react";
import Modal from 'react-modal';
import {useDispatch, useSelector} from "react-redux";
import {actionCountriesFetchAll, actionCountriesSet} from "../../store/Actions/countriesActions";
import {actionShipmentsFetchAll} from "../../store/Actions/shipmentsActions";
import {ShipmentsAPI} from "../../API/ShipmentsAPI";
import {AccountAPI} from "../../API/AccountAPI";

const customStyles = {
	content: {
		background: 'cornflowerblue',
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
};

const Dashboard = () => {
	const username = KeycloakService.getUsername()
	const token = KeycloakService.getToken()
	const dispatch = useDispatch()
	const { countries } = useSelector(state => state.countries)
	const { shipments } = useSelector(state => state.shipments)
	const [completeShipments, setCompleteShipments] = useState([]);
	const [profile, setProfile] = useState("")
	let subtitle;
	const [modalIsOpen, setIsOpen] = React.useState(false);
	Modal.setAppElement('#root');


	function openModal() {
		setIsOpen(true);
	}
	function afterOpenModal() {
		// references are now sync'd and can be accessed.
		subtitle.style.color = '#ffffff';
	}

	function closeModal() {
		setIsOpen(false);
	}

	useEffect(async () => {
		let profile = await AccountAPI.login(token)
		console.log(profile);
		console.log(KeycloakService.hasRole(["Administrator"]))
		setProfile(profile);
		if (countries.length === 0)
			dispatch(actionCountriesFetchAll())
		if (shipments.length === 0)
			dispatch(actionShipmentsFetchAll())
		setCompleteShipments(await ShipmentsAPI.getCompleteAccountShipments(token))
	}, []);

	const updateShipments = async () => {
		dispatch(actionCountriesSet(await ShipmentsAPI.getAccountShipments(token)))
	}

	const order = async (evt) => {
		evt.preventDefault();
		let recName = evt.target.recName.value
		let weight = evt.target.weight.value
		let color = evt.target.color.value
		let country = evt.target.country.value
		await ShipmentsAPI.orderPackage({ recName, weight, color, country, token })
		closeModal();
		await updateShipments();
	}

	return (
		<div className="container border p-3 my-3">
			<main>
				<div>
					<h1>Welcome to the Dashboard, { username }</h1>
				</div>
				<h3>SHIPMENTS UNDER WAY</h3>
				<table className="table">
					<tbody>
					<tr>
						<th>Box nr</th>
						<th>Receiver</th>
						<th>Weight</th>
						<th>Box color</th>
						<th>Status</th>
						<th>Country</th>
						<th>Cost</th>
					</tr>
					{ shipments.map((ship, index) => <tr key={index} style={{backgroundColor: ship.boxColor}}>
						<th>{ index + 1} </th>
						<th>{ ship.receiverName} </th>
						<th>{ ship.weight} </th>
						<th>{ ship.boxColor} </th>
						<th>{ ship.status } </th>
						<th>{ ship.countryName } </th>
						<th>{ ship.cost }</th>
					</tr>) }
					</tbody>
				</table>

				<h3>COMPLETED SHIPMENTS</h3>
				<table className="table">
					<tbody>
					<tr>
						<th>Box nr</th>
						<th>Receiver</th>
						<th>Weight</th>
						<th>Box color</th>
						<th>Status</th>
						<th>Country</th>
						<th>Cost</th>
					</tr>
					{ completeShipments.map((ship, index) => <tr key={index} style={{backgroundColor: ship.boxColor}}>
						<td>{ index + 1} </td>
						<td>{ ship.receiverName} </td>
						<td>{ ship.weight} </td>
						<td>{ ship.boxColor} </td>
						<td>{ ship.status } </td>
						<td>{ ship.countryName } </td>
						<td>{ ship.cost }</td>
					</tr>) }
					</tbody>
				</table>

				<button type="button" className="btn btn-primary btn-lg" onClick={openModal}>Create New Shipment</button>
				<div>
					<Modal
						isOpen={modalIsOpen}
						onAfterOpen={afterOpenModal}
						onRequestClose={closeModal}
						style={customStyles}
						contentLabel="Example Modal"
					>
						<h2 ref={(_subtitle) => (subtitle = _subtitle)}>Send new package</h2>

						<div>Enter details for shipment</div>
						<form onSubmit={order} className="orderForm">
							<input name="recName" className="newPackage" placeholder=" Enter receiver name"/>
							<select  name="weight" defaultValue={"Weight of package"}>
								<option value="Basic" name="Basic">Basic</option>
								<option value="Humble" name="Humble">Humble</option>
								<option value="Deluxe" name="Deluxe">Deluxe</option>
								<option value="Premium" name="Premium">Premium</option>
							</select>
							<input type="color" name="color" placeholder="Color" />
							<select className="newPackage" name="country">
								{ countries.map((country, index) => <option value={country.name} key={index}>
									{ country.name }
								</option>) }
							</select><br/>
							<button type="submit" className="shipButtons" id="sendButton">Send</button>
							<button className="shipButtons" id="closeButton" onClick={closeModal}>Close</button>
						</form>
					</Modal>
				</div>
			</main>
		</div>

	)
}
export default withKeycloak(Dashboard)
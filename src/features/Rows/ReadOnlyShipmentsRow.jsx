import React from "react";

const ReadOnlyShipmentsRow = ({ shipment, index, handleEditClick, handleDeleteClick }) => {
    return (
        <tr>
            <td>{ index + 1} </td>
            <td>{ shipment.receiverName } </td>
            <td>{ shipment.weight} </td>
            <td>{ shipment.boxColor} </td>
            <td>{ shipment.status } </td>
            <td>{ shipment.countryName } </td>
            <td>{ shipment.cost }</td>
            <td>
                <button
                    className="btn btn-primary"
                    type="button"
                    onClick={(event) => handleEditClick(event, shipment)}
                >
                    Edit
                </button>
                <button className="btn btn-primary" type="button" onClick={() => handleDeleteClick(shipment.id)}>
                    Delete
                </button>
            </td>
        </tr>
    );
};

export default ReadOnlyShipmentsRow;
// TYPES
export const ACTION_COUNTRIES_SET = '[countries] SET';
export const ACTION_COUNTRIES_FETCH_ALL = '[countries] FETCH_ALL';

// SET
export const actionCountriesSet = payload => ({
    type: ACTION_COUNTRIES_SET,
    payload
})

// FETCH
export const actionCountriesFetchAll = () => ({
    type: ACTION_COUNTRIES_FETCH_ALL
})

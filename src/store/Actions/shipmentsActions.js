// TYPES
export const ACTION_SHIPMENTS_SET = '[shipments] SET';
export const ACTION_SHIPMENTS_FETCH_ALL = '[shipments] FETCH_ALL';

// SET
export const actionShipmentsSet = payload => ({
    type: ACTION_SHIPMENTS_SET,
    payload
})

// FETCH
export const actionShipmentsFetchAll = () => ({
    type: ACTION_SHIPMENTS_FETCH_ALL
})
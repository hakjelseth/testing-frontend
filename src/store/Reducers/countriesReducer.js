import {ACTION_COUNTRIES_FETCH_ALL, ACTION_COUNTRIES_SET} from "../Actions/countriesActions";

const initialState = {
    countries: [],
    error: ''
}

export const countriesReducer = (state =  initialState, action) => {
    switch (action.type) {
        case ACTION_COUNTRIES_SET:
            return {
                error: '',
                countries: action.payload
            }

        case ACTION_COUNTRIES_FETCH_ALL:
            return {
                ...state,
                error: ''
            }

        default:
            return state;
    }
}
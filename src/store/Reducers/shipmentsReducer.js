import {ACTION_SHIPMENTS_FETCH_ALL, ACTION_SHIPMENTS_SET} from "../Actions/shipmentsActions";

const initialState = {
    shipments: [],
    error: ''
}

export const shipmentsReducer = (state =  initialState, action) => {
    switch (action.type) {
        case ACTION_SHIPMENTS_SET:
            return {
                error: '',
                shipments: action.payload
            }

        case ACTION_SHIPMENTS_FETCH_ALL:
            return {
                ...state,
                error: ''
            }

        default:
            return state;
    }
}
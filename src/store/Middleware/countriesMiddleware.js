import {ACTION_COUNTRIES_FETCH_ALL, actionCountriesSet} from "../Actions/countriesActions";
import {CountriesAPI} from "../../API/CountriesAPI";

export const countriesMiddleware = ({ dispatch }) => next => action => {

    next(action)
    if (action.type === ACTION_COUNTRIES_FETCH_ALL) {
        CountriesAPI.getCountries()
            .then( countries => {
                dispatch( actionCountriesSet(countries ))
            })
            /*.catch(error => {
                dispatch( countriesErrorAction( error.message ) )
            })*/
    }
}
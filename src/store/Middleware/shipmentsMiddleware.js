import KeycloakService from "../../services/KeycloakService";
import {ACTION_SHIPMENTS_FETCH_ALL, actionShipmentsSet} from "../Actions/shipmentsActions";
import {ShipmentsAPI} from "../../API/ShipmentsAPI";

export const shipmentsMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_SHIPMENTS_FETCH_ALL) {
        ShipmentsAPI.getAccountShipments(KeycloakService.getToken())
            .then( shipments => {
                dispatch( actionShipmentsSet(shipments ))
            })
        /*.catch(error => {
            dispatch( countriesErrorAction( error.message ) )
        })*/
    }
}
import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {countriesMiddleware} from "./Middleware/countriesMiddleware";
import {countriesReducer} from "./Reducers/countriesReducer";
import {shipmentsReducer} from "./Reducers/shipmentsReducer"
import {shipmentsMiddleware} from "./Middleware/shipmentsMiddleware";
const rootReducers = combineReducers({
    countries: countriesReducer,
    shipments: shipmentsReducer
})

const store = createStore(rootReducers, composeWithDevTools(
    applyMiddleware(countriesMiddleware),
    applyMiddleware(shipmentsMiddleware)
))

export default store

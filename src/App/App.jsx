import "./App.css";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "../features/Dashboard/Dashboard";
import Login from "../features/Login/Login";
import Navbar from "../features/Navbar/Navbar";
import Profile from "../features/Profile/Profile";
import Admin from "../features/Admin/Admin";
import GuardedRoute from "../features/Guard/AdminRoute";
import UserRoute from "../features/Guard/UserRoute";
import LoggedInRoute from "../features/Guard/LoggedInRoute";

const App = () => {

	return (
		<BrowserRouter>
			<Navbar />
			<Switch>
				<Route exact path="/">
					<Redirect to="/login" />
				</Route>
				<LoggedInRoute path="/login" component={Login} />
				<UserRoute path="/dashboard" component={Dashboard} />
				<UserRoute path="/profile" component={Profile} />
				<GuardedRoute path="/admin" component={Admin}/>
			</Switch>
		</BrowserRouter>
	)
}

export default App;
